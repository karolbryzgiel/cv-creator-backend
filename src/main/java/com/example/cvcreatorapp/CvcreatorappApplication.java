package com.example.cvcreatorapp;

import repositories.EmployerRepository;
import entity.EmployerEntity;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
@SpringBootApplication
@EntityScan( basePackages = {"entity"} )
@ComponentScan( basePackages = { "controllers", "repositories", "exceptions"})
@EnableJpaRepositories("repositories")

public class CvcreatorappApplication {

    public static void main(String[] args) {
        SpringApplication.run(CvcreatorappApplication.class, args);
    }
}
