package exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.*;

@ControllerAdvice
public class ExceptionHandlingController {

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ExceptionResponse> handleConstraintViolation(ConstraintViolationException ex) {
        Set<ConstraintViolation<?>> constraintViolations = new HashSet<ConstraintViolation<?>>();
        List<String> errorList = new ArrayList<String>();
        Map<String, String> errorMap = new HashMap<String, String>();
        constraintViolations = ex.getConstraintViolations();
        ExceptionResponse response = new ExceptionResponse();
        for (ConstraintViolation violation : constraintViolations) {
            String violationPath[] = (violation.getPropertyPath().toString()).split("\\.");
            errorMap.put(violationPath[2], violation.getMessage());
            errorList.add(violation.getMessage());
        }
        //errors.forEach((k, v) -> System.out.println("Key: " +k +" // Value: " + v));
        response.setErrorCode("Bad input");
        response.setErrorMessage("Error with inputs validation.");
        response.setErrorList(errorList);
        response.setErrorMap(errorMap);
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
    }

}
