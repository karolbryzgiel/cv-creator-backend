package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;


@ResponseStatus(HttpStatus.BAD_REQUEST)
public class PeselNotValidException extends Exception {

    public PeselNotValidException(String message) {
        super(message);
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<?> peselValid(PeselNotValidException ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Bad input");
        response.setErrorMessage("Error with inputs validation.");
        Map<String, String> peselMap = new HashMap<String,String>();
        peselMap.put("pesel", "Pesel is not valid");
        response.setErrorMap(peselMap);
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);

    }

}
