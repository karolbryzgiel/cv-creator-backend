package exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

@ResponseStatus(HttpStatus.NOT_FOUND) // 404
public class ObjectNotFoundException extends IllegalArgumentException {

    public ObjectNotFoundException(int id) {
        super("Object with id: " +id +" not found");
    }

}
