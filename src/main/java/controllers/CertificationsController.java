package controllers;


import exceptions.ObjectNotFoundException;
import entity.CertificationsEntity;
import entity.EmployerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.CertificationsRepository;
import repositories.EmployerRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class CertificationsController {

    @Autowired
    CertificationsRepository certificationsRepository;
    @Autowired
    EmployerRepository employerRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employers/{employerId}/certification")
    public List<CertificationsEntity> getCertification(@PathVariable("employerId") int employerId) {
        List<CertificationsEntity> certificationList = new ArrayList<>();

        try {
            EmployerEntity employer = employerRepository.findById(employerId).get();
            for (CertificationsEntity entity : certificationsRepository.findAll()) {
                if (entity.getEmployerByEmployerId() == employer) {
                    certificationList.add(entity);
                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return certificationList;
    }

    @Transactional
    @PostMapping("/employers/{employerId}/certification")
    public CertificationsEntity saveCertification(@PathVariable("employerId") int employerId,
                                                  @Valid @RequestBody CertificationsEntity certification , BindingResult bindingResult) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        certification.setEmployerByEmployerId(employer);
        certificationsRepository.save(certification);
        return certification;
    }

    @DeleteMapping("/employers/{employerId}/certification/{certificationId}")
    public ResponseEntity<?> deleteCertification(@PathVariable("employerId") int employerId, @PathVariable("certificationId") int certificationId) {
        CertificationsEntity certificationToDelete = certificationsRepository.findById(certificationId).orElseThrow(() -> new ObjectNotFoundException(certificationId));
        certificationsRepository.delete(certificationToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/employers/{employerId}/certification/{certificationId}")
    public CertificationsEntity updateCertification(@PathVariable("employerId") int employerId, @PathVariable("certificationId") int certificationId, @RequestBody CertificationsEntity certification) {
        CertificationsEntity certificationToEdit = certificationsRepository.findById(certificationId).orElseThrow(() -> new ObjectNotFoundException(certificationId));
        certificationToEdit.setDateofGet(certification.getDateofGet());
        certificationToEdit.setName(certification.getName());
        certificationsRepository.save(certificationToEdit);
        return certificationToEdit;
    }

}
