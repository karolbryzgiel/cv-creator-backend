package controllers;

import exceptions.ObjectNotFoundException;
import entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import repositories.EmployerProjectsRepository;
import repositories.EmployerRepository;
import repositories.ProjectsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class EmployerProjectsController {

    @Autowired
    EmployerProjectsRepository employerProjectsRepository;
    @Autowired
    ProjectsRepository projectsRepository;
    @Autowired
    EmployerRepository employerRepository;

    @GetMapping("/employer/{employerId}/projects")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<EmployerProjectsEntity>> getEmployerProjects(@PathVariable("employerId") int employerId) {
        List<EmployerProjectsEntity> employerProjectsList = new ArrayList<>();

        try {
            EmployerEntity employer = employerRepository.findById(employerId).get();
            ProjectsEntity projects = projectsRepository.findById(0).get();
            for (EmployerProjectsEntity entity : employerProjectsRepository.findAll()) {
                employerProjectsList.add(entity);
//                if (entity.getEmployerByEmployerId() == employer) {
//                    employerProjectsList.add(entity);
//                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return new ResponseEntity<List<EmployerProjectsEntity>>(employerProjectsList, HttpStatus.OK);
    }

    @PostMapping("/employer/{employerId}/projects/{projectId}")
    public EmployerProjectsEntity saveEducation(@PathVariable("projectId") int projectId,
                                                @PathVariable("employerId") int employerId,
                                                @RequestBody EmployerProjectsEntity employerProjects) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        ProjectsEntity project = projectsRepository.findById(projectId).orElseThrow(() -> new ObjectNotFoundException(projectId));
        employerProjects.setEmployerByEmployerId(employer);
        employerProjects.setProjectsByProjectsId(project);
        employerProjects.setEmployerId(employerId);
        employerProjects.setProjectsId(projectId);
        employerProjectsRepository.save(employerProjects);
        return employerProjects;
    }

    @DeleteMapping("/employer/{employerId}/projects/{projectId}")
    public ResponseEntity<?> deleteEducation(@PathVariable("projectId") int projectId,
                                             @PathVariable("employerId") int employerId) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        ProjectsEntity project = projectsRepository.findById(projectId).orElseThrow(() -> new ObjectNotFoundException(projectId));
        EmployerProjectsEntity employerProjectsToDelete = employerProjectsRepository.findById(employerId).get();

        if( employerProjectsToDelete.getProjectsByProjectsId() == project) {
            employerProjectsRepository.delete(employerProjectsToDelete);
        }

        return ResponseEntity.ok().build();
    }


    @PutMapping("/employer/{employerId}/projects/{projectId}")
    public EmployerProjectsEntity updateEmployer(@PathVariable("projectId") int projectId,
                                          @PathVariable("employerId") int employerId,
                                          @RequestBody EmployerProjectsEntity employerProjects) {
        ProjectsEntity project = projectsRepository.findById(projectId).orElseThrow(() -> new ObjectNotFoundException(projectId));
        EmployerProjectsEntity employerProjectsToEdit = employerProjectsRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        if( employerProjectsToEdit.getProjectsByProjectsId() == project) {
            employerProjectsToEdit.setFunction(employerProjects.getFunction());
            employerProjectsToEdit.setStartDate(employerProjects.getStartDate());
            employerProjectsToEdit.setEndDate(employerProjects.getEndDate());
        }
        employerProjectsRepository.save(employerProjectsToEdit);
        return employerProjectsToEdit;
    }

}
