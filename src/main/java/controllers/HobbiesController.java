package controllers;


import exceptions.ObjectNotFoundException;
import entity.EmployerEntity;
import entity.HobbiesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.EmployerRepository;
import repositories.HobbiesRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class HobbiesController {

    @Autowired
    HobbiesRepository hobbiesRepository;
    @Autowired
    EmployerRepository employerRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employers/{employerId}/hobbies")
    public List<HobbiesEntity> getHobbies(@PathVariable("employerId") int employerId) {
        List<HobbiesEntity> hobbiesList = new ArrayList<>();

        try {
            EmployerEntity employer = employerRepository.findById(employerId).get();
            for (HobbiesEntity entity : hobbiesRepository.findAll()) {
                if (entity.getEmployerByEmployerId() == employer) {
                    hobbiesList.add(entity);
                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return hobbiesList;
    }

    @Transactional
    @PostMapping("/employers/{employerId}/hobbies")
    public HobbiesEntity saveHobbies(@PathVariable("employerId") int employerId,
                                     @Valid @RequestBody HobbiesEntity hobby, BindingResult bindingResult) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        hobby.setEmployerByEmployerId(employer);
        hobbiesRepository.save(hobby);
        return hobby;
    }

    @DeleteMapping("/employers/{employerId}/hobbies/{hobbyId}")
    public ResponseEntity<?> deleteHobbies(@PathVariable("employerId") int employerId, @PathVariable("hobbyId") int hobbieId) {
        HobbiesEntity hobbyToDelete = hobbiesRepository.findById(hobbieId).orElseThrow(() -> new ObjectNotFoundException(hobbieId));
        hobbiesRepository.delete(hobbyToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/employers/{employerId}/hobbies/{hobbyId}")
    public HobbiesEntity updateHobbies(@PathVariable("employerId") int employerId, @PathVariable("hobbyId") int hobbyId, @RequestBody HobbiesEntity hobby) {
        HobbiesEntity hobbyToEdit = hobbiesRepository.findById(hobbyId).orElseThrow(() -> new ObjectNotFoundException(hobbyId));
        hobbyToEdit.setCategory(hobby.getCategory());
        hobbyToEdit.setName(hobby.getName());
        hobbiesRepository.save(hobbyToEdit);
        return hobbyToEdit;
    }

}
