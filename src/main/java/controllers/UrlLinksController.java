package controllers;


import exceptions.ObjectNotFoundException;
import entity.EmployerEntity;
import entity.UrlLinksEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.EmployerRepository;
import repositories.UrlLinksRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class UrlLinksController {

    @Autowired
    UrlLinksRepository urlLinksRepository;
    @Autowired
    EmployerRepository employerRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employers/{employerId}/urlLinks")
    public List<UrlLinksEntity> getUrlLinks(@PathVariable("employerId") int employerId) {
        List<UrlLinksEntity> urlLinksList = new ArrayList<>();

        try {
            EmployerEntity employer = employerRepository.findById(employerId).get();
            for (UrlLinksEntity entity : urlLinksRepository.findAll()) {
                if (entity.getEmployerByEmployerId() == employer) {
                    urlLinksList.add(entity);
                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return urlLinksList;
    }

    @Transactional
    @PostMapping("/employers/{employerId}/urlLinks")
    public UrlLinksEntity saveUrlLinks(@PathVariable("employerId") int employerId,
                                       @Valid @RequestBody UrlLinksEntity urlLinks, BindingResult bindingResult) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        urlLinks.setEmployerByEmployerId(employer);
        urlLinksRepository.save(urlLinks);
        return urlLinks;
    }

    @DeleteMapping("/employers/{employerId}/urlLinks/{urlLinksId}")
    public ResponseEntity<?> deleteUrlLinks(@PathVariable("employerId") int employerId, @PathVariable("urlLinksId") int urlLinksId) {
        UrlLinksEntity urlLinksToDelete = urlLinksRepository.findById(urlLinksId).orElseThrow(() -> new ObjectNotFoundException(urlLinksId));
        urlLinksRepository.delete(urlLinksToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/employers/{employerId}/urlLinks/{urlLinksId}")
    public UrlLinksEntity updateUrlLinks(@PathVariable("employerId") int employerId, @PathVariable("urlLinksId") int urlLinksId, @RequestBody UrlLinksEntity urlLinks) {
        UrlLinksEntity urlLinksToEdit = urlLinksRepository.findById(urlLinksId).orElseThrow(() -> new ObjectNotFoundException(urlLinksId));
        urlLinksToEdit.setLinkName(urlLinks.getLinkName());
        urlLinksToEdit.setUrl(urlLinks.getUrl());
        urlLinksRepository.save(urlLinksToEdit);
        return urlLinksToEdit;
    }

}
