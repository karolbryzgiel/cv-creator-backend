package controllers;


import exceptions.ObjectNotFoundException;
import entity.ExperienceEntity;
import entity.EmployerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.ExperienceRepository;
import repositories.EmployerRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class ExperienceController {

    @Autowired
    ExperienceRepository experienceRepository;
    @Autowired
    EmployerRepository employerRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employers/{employerId}/experience")
    public ResponseEntity<List<ExperienceEntity>> getExperience(@PathVariable("employerId") int employerId) {
        List<ExperienceEntity> experienceList = new ArrayList<>();
        EmployerEntity employer = employerRepository.findById(employerId).get();
        for (ExperienceEntity entity : experienceRepository.findAll()) {
            if (entity.getEmployerByEmployerId() == employer) {
                experienceList.add(entity);
            }
        }
        if ( experienceList.isEmpty()) {
            return new ResponseEntity<List<ExperienceEntity>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<ExperienceEntity>>(experienceList, HttpStatus.OK);
    }

    @Transactional
    @PostMapping("/employers/{employerId}/experience")
    public ExperienceEntity saveExperience(@PathVariable("employerId") int employerId,
                                           @Valid @RequestBody ExperienceEntity experience, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return experience;
        }
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        experience.setEmployerByEmployerId(employer);
        experienceRepository.save(experience);
        return experience;
    }

    @DeleteMapping("/employers/{employerId}/experience/{experienceId}")
    public ResponseEntity<?> deleteExperience(@PathVariable("employerId") int employerId, @PathVariable("experienceId") int experienceId) {
        ExperienceEntity experienceToDelete = experienceRepository.findById(experienceId).orElseThrow(() -> new ObjectNotFoundException(experienceId));
        experienceRepository.delete(experienceToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/employers/{employerId}/experience/{experienceId}")
    public ExperienceEntity updateExperience(@PathVariable("employerId") int employerId, @PathVariable("experienceId") int experienceId, @RequestBody ExperienceEntity experience) {
        ExperienceEntity experienceToEdit = experienceRepository.findById(experienceId).orElseThrow(() -> new ObjectNotFoundException(experienceId));
        experienceToEdit.setStartDate(experience.getStartDate());
        experienceToEdit.setEndDate(experience.getEndDate());
        experienceToEdit.setCompanyName(experience.getCompanyName());
        experienceToEdit.setPosition(experience.getPosition());
        experienceRepository.save(experienceToEdit);
        return experienceToEdit;
    }

}
