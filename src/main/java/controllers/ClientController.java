package controllers;

import ch.qos.logback.core.net.server.Client;
import entity.CertificationsEntity;
import entity.ClientEntity;
import entity.EmployerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.ClientRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class ClientController {

    @Autowired
    private ClientRepository clientRepository;

    @RequestMapping(path = "/clients", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.ACCEPTED)
    public ClientEntity saveClient(@Valid @RequestBody ClientEntity user, BindingResult bindingResult) {
        clientRepository.save(user);
        return user;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/clients")
    public List<ClientEntity> getClients() {
        List<ClientEntity> clientList = new ArrayList<ClientEntity>();
        for (ClientEntity entity : clientRepository.findAll()) {
            clientList.add(entity);
        }

        return clientList;
    }

    @DeleteMapping("/clients/{clientId} ")
    public ResponseEntity<?> deleteClient(@PathVariable("clientId") int clientId) {
        ClientEntity clientToDelete = clientRepository.findById(clientId).orElseThrow(() -> new IllegalArgumentException("Not Found"));
        clientRepository.delete(clientToDelete);
        return new ResponseEntity<ClientEntity>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/clients/{clientId}")
    public ClientEntity getClient(@PathVariable("clientId") int clientId) {
        ClientEntity client = clientRepository.findById(clientId).get();
        return client;
    }

    @Transactional
    @PutMapping("/clients/{clientId}")
    public ClientEntity updateClient(@PathVariable("clientId") int clientId, @RequestBody ClientEntity client) {
        ClientEntity clientToEdit = clientRepository.findById(clientId).get();
        clientToEdit.setAddress(client.getAddress());
        clientToEdit.setName(client.getName());
        clientRepository.save(clientToEdit);
        return clientToEdit;
    }

}
