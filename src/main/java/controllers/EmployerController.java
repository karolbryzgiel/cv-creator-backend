package controllers;


import entity.EmployerEntity;
import exceptions.ExceptionResponse;
import exceptions.ObjectNotFoundException;
import exceptions.PeselNotValidException;
import exceptions.PeselValidiator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.EmployerRepository;

import javax.validation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class EmployerController {

    public static final Logger logger = LoggerFactory.getLogger(EmployerController.class);

    @Autowired
    private EmployerRepository employerRepository;

    @RequestMapping(path = "/employer", method = RequestMethod.POST)
    public ResponseEntity<?> saveEmployer(@Valid @RequestBody EmployerEntity employer, BindingResult bindingResult) {
        PeselValidiator peselValidiator = new PeselValidiator(employer.getPesel());
        try {
            peselValidiator.isValid();
        } catch (PeselNotValidException ex) {
            return new ResponseEntity<ExceptionResponse>(responseForPeselNotValid(), HttpStatus.NOT_FOUND);
        }
        employerRepository.save(employer);
        return new ResponseEntity<EmployerEntity>(employer, HttpStatus.OK);
    }


    @DeleteMapping("/employer/{id}")
    public ResponseEntity<?> deleteEmployer(@PathVariable("id") int employerId) {
        EmployerEntity employerToDelete = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        employerRepository.delete(employerToDelete);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/employer/{employerId}")
    public ResponseEntity<?> updateEmployer(@PathVariable("employerId") int employerId, @Valid @RequestBody EmployerEntity employer, BindingResult bindingResult){
        EmployerEntity employerToEdit = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        PeselValidiator peselValidiator = new PeselValidiator(employer.getPesel());
        try {
            peselValidiator.isValid();
        } catch (PeselNotValidException ex) {
            return new ResponseEntity<ExceptionResponse>(responseForPeselNotValid(), HttpStatus.NOT_FOUND);
        }
        try {
            employerToEdit.setAddress(employer.getAddress());
            employerToEdit.setPesel(employer.getPesel());
            employerToEdit.setName(employer.getName());
            employerToEdit.setSurname(employer.getSurname());
            employerToEdit.setBirthDate(employer.getBirthDate());
            employerToEdit.setPhoneNumber(employer.getPhoneNumber());
            employerToEdit.setEmailAddress(employer.getEmailAddress());
            employerToEdit.setHiredate(employer.getHiredate());
        }
        catch( ConstraintViolationException ex) {
            ex.printStackTrace();
        }
        updateEditedEmployer(employerToEdit);
        return new ResponseEntity<EmployerEntity>(employerToEdit, HttpStatus.OK);
    }

    public void updateEditedEmployer(@Valid @RequestBody EmployerEntity employer) {
        employerRepository.save(employer);
    }


    @GetMapping("/employers")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<List<EmployerEntity>> getEmployers() {
        List<EmployerEntity> employerList = new ArrayList<>();
        for (EmployerEntity entity : employerRepository.findAll()) {
            employerList.add(entity);
        }
        if (employerList.isEmpty()) {
            return new ResponseEntity<List<EmployerEntity>>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<List<EmployerEntity>>(employerList, HttpStatus.OK);
    }

    @GetMapping("/employer/{employerId}")
    public ResponseEntity<?> getEmployer(@PathVariable("employerId") int employerId) {
        if (employerRepository.findById(employerId) == null) {
            throw new ObjectNotFoundException(employerId);
        }
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        return new ResponseEntity<EmployerEntity>(employer, HttpStatus.OK);
    }

    public ExceptionResponse responseForPeselNotValid() {
        ExceptionResponse response = new ExceptionResponse();
        Map<String, String> peselMap = new HashMap<String, String>();
        peselMap.put("pesel", "Pesel is not valid");
        List<String> peselList = new ArrayList<String>();
        peselList.add("Pesel is not valid");

        response.setErrorCode("Bad input");
        response.setErrorMessage("Error with inputs validation.");
        response.setErrorMap(peselMap);
        response.setErrorList(peselList);
        return response;
    }

}
