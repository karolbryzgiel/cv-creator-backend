package controllers;

import exceptions.ObjectNotFoundException;
import entity.EducationEntity;
import entity.EmployerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.EducationRepository;
import repositories.EmployerRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class EducationController {

    @Autowired
    EducationRepository educationRepository;
    @Autowired
    EmployerRepository employerRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employers/{employerId}/education")
    public List<EducationEntity> getEducations(@PathVariable("employerId") int employerId) {
        List<EducationEntity> educationList = new ArrayList<>();
        try {
            EmployerEntity employer = employerRepository.findById(employerId).get();
            for (EducationEntity entity : educationRepository.findAll()) {
                if (entity.getEmployerByEmployerId() == employer) {
                    educationList.add(entity);
                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return educationList;
    }

    @Transactional
    @PostMapping("/employers/{employerId}/education")
    public ResponseEntity<?> saveEducation(@PathVariable("employerId") int employerId,
                                         @Valid @RequestBody EducationEntity education,BindingResult bindingResult) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(null);
        education.setEmployerByEmployerId(employer);
        educationRepository.save(education);
        return new ResponseEntity<EducationEntity>(education, HttpStatus.OK);
    }

    @DeleteMapping("/employers/{employerId}/education/{educationId}")
    public ResponseEntity<?> deleteEducation(@PathVariable("employerId") int employerId, @PathVariable("educationId") int educationId) {
        EducationEntity educationToDelete = educationRepository.findById(educationId).orElseThrow(() -> new ObjectNotFoundException(educationId));
        educationRepository.delete(educationToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/employers/{employerId}/education/{educationId}")
    public EducationEntity updateEmployer(@PathVariable("employerId") int employerId, @PathVariable("educationId") int educationId, @RequestBody EducationEntity education) {
        EducationEntity educationToEdit = educationRepository.findById(educationId).orElseThrow(() -> new ObjectNotFoundException(educationId));
        educationToEdit.setStartDate(education.getStartDate());
        educationToEdit.setEndDate(education.getEndDate());
        educationToEdit.setDirection(education.getDirection());
        educationToEdit.setNameOfOrganisation(education.getNameOfOrganisation());
        educationToEdit.setProfessionalTitle(education.getProfessionalTitle());
        educationToEdit.setSpecialization(education.getSpecialization());
        educationRepository.save(educationToEdit);
        return educationToEdit;
    }

}
