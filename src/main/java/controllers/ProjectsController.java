package controllers;

import exceptions.ObjectNotFoundException;
import entity.ClientEntity;
import entity.ProjectsEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.ClientRepository;
import repositories.ProjectsRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class ProjectsController {

    @Autowired
    ProjectsRepository projectsRepository;
    @Autowired
    ClientRepository clientRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/clients/{clientId}/projects")
    public List<ProjectsEntity> getProjects(@PathVariable("clientId") int clientId) {
        List<ProjectsEntity> projectsList = new ArrayList<>();

        try {
            ClientEntity client = clientRepository.findById(clientId).get();
            for (ProjectsEntity entity : projectsRepository.findAll()) {
                if (entity.getClientById() == client) {
                    projectsList.add(entity);
                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return projectsList;
    }

    @Transactional
    @PostMapping("/clients/{clientId}/projects")
    public ProjectsEntity saveProjects(@PathVariable("clientId") int clientId,
                                       @Valid @RequestBody ProjectsEntity projects, BindingResult bindingResult) {
        ClientEntity client = clientRepository.findById(clientId).orElseThrow(() -> new ObjectNotFoundException(clientId));
        projects.setClientById(client);
        projectsRepository.save(projects);
        return projects;
    }

    @DeleteMapping("/clients/{clientId}/projects/{projectsId}")
    public ResponseEntity<?> deleteProjects(@PathVariable("clientId") int clientId, @PathVariable("projectsId") int projectsId) {
        ProjectsEntity projectsToDelete = projectsRepository.findById(projectsId).orElseThrow(() -> new ObjectNotFoundException(projectsId));
        projectsRepository.delete(projectsToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/clients/{clientId}/projects/{projectsId}")
    public ProjectsEntity updateProjects(@PathVariable("clientId") int clientId, @PathVariable("projectsId") int projectsId, @RequestBody ProjectsEntity projects) {
        ProjectsEntity projectsToEdit = projectsRepository.findById(projectsId).orElseThrow(() -> new ObjectNotFoundException(projectsId));
        projectsToEdit.setStartDate(projects.getStartDate());
        projectsToEdit.setEndDate(projects.getEndDate());
        projectsToEdit.setDescription(projects.getDescription());
        projectsToEdit.setProjectName(projects.getProjectName());
        projectsRepository.save(projectsToEdit);
        return projectsToEdit;
    }

}
