package controllers;

import exceptions.ObjectNotFoundException;
import entity.SkillsEntity;
import entity.EmployerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import repositories.EmployerRepository;
import repositories.SkillsRepository;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@Validated
public class SkillsController {

    @Autowired
    SkillsRepository skillsRepository;
    @Autowired
    EmployerRepository employerRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employers/{employerId}/skills")
    public List<SkillsEntity> getSkills(@PathVariable("employerId") int employerId) {
        List<SkillsEntity> skillsList = new ArrayList<>();

        try {
            EmployerEntity employer = employerRepository.findById(employerId).get();
            for (SkillsEntity entity : skillsRepository.findAll()) {
                if (entity.getEmployerByEmployerId() == employer) {
                    skillsList.add(entity);
                }
            }
        } catch (ResourceNotFoundException ex) {
            ex.printStackTrace();
        } catch (NoSuchElementException ex) {
            System.out.println(ex.getMessage());
        }

        return skillsList;
    }

    @Transactional
    @PostMapping("/employers/{employerId}/skill")
    public SkillsEntity saveSkills(@PathVariable("employerId") int employerId,
                                   @Valid @RequestBody SkillsEntity skill, BindingResult bindingResult) {
        EmployerEntity employer = employerRepository.findById(employerId).orElseThrow(() -> new ObjectNotFoundException(employerId));
        skill.setEmployerByEmployerId(employer);
        skillsRepository.save(skill);
        return skill;
    }

    @DeleteMapping("/employers/{employerId}/skill/{skillId}")
    public ResponseEntity<?> deleteSkills(@PathVariable("employerId") int employerId, @PathVariable("skillId") int skillId) {
        SkillsEntity skillToDelete = skillsRepository.findById(skillId).orElseThrow(() -> new ObjectNotFoundException(skillId));
        skillsRepository.delete(skillToDelete);
        return ResponseEntity.ok().build();
    }


    @Transactional
    @PutMapping("/employers/{employerId}/skill/{skillId}")
    public SkillsEntity updateSkills(@PathVariable("employerId") int employerId, @PathVariable("skillId") int skillId, @RequestBody SkillsEntity skill) {
        SkillsEntity skillToEdit = skillsRepository.findById(skillId).orElseThrow(() -> new ObjectNotFoundException(skillId));
        skillToEdit.setLevel(skill.getLevel());
        skillToEdit.setName(skill.getName());
        skillsRepository.save(skillToEdit);
        return skillToEdit;
    }

}
