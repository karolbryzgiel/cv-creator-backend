package repositories;

import entity.EmployerProjectsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EmployerProjectsRepository extends CrudRepository<EmployerProjectsEntity, Integer> {

}
