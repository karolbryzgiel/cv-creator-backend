package repositories;

import entity.HobbiesEntity;
import org.springframework.data.repository.CrudRepository;

public interface HobbiesRepository extends CrudRepository<HobbiesEntity, Integer> {
}
