package repositories;

import entity.ProjectsEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProjectsRepository extends CrudRepository<ProjectsEntity, Integer> {
}
