package repositories;

import entity.SkillsEntity;
import org.springframework.data.repository.CrudRepository;

public interface SkillsRepository extends CrudRepository<SkillsEntity, Integer> {

}
