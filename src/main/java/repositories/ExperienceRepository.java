package repositories;

import entity.ExperienceEntity;
import org.springframework.data.repository.CrudRepository;

public interface ExperienceRepository extends CrudRepository<ExperienceEntity, Integer> {
}
