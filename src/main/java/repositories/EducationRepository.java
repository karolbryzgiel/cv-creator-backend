package repositories;

import entity.EducationEntity;
import entity.EmployerEntity;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EducationRepository extends CrudRepository <EducationEntity, Integer> {


    //List<EducationEntity> findByEmployerId(int employerId);

}
