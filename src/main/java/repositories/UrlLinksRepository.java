package repositories;

import entity.UrlLinksEntity;
import org.springframework.data.repository.CrudRepository;

public interface UrlLinksRepository extends CrudRepository<UrlLinksEntity, Integer> {
}
