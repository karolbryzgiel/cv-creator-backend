package repositories;

import entity.CertificationsEntity;
import org.springframework.data.repository.CrudRepository;

public interface CertificationsRepository extends CrudRepository<CertificationsEntity, Integer> {


    //List<CertificationsEntity> findByName(String name);
}
