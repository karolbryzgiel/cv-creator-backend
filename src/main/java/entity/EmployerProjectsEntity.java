package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "Employer_Projects", schema = "", catalog = "")
@IdClass(EmployerProjectsEntityPK.class)
public class EmployerProjectsEntity {
    private int employerId;
    private int projectsId;
    private String function;
    private Date startDate;
    private Date endDate;
    private EmployerEntity employerByEmployerId;
    private ProjectsEntity projectsByProjectsId;

    public EmployerProjectsEntity(int employerId, int projectsId, String function, Date startDate, Date endDate) {
        this.employerId = employerId;
        this.projectsId = projectsId;
        this.function = function;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public EmployerProjectsEntity() {
    }


    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "EmployerID")
    public int getEmployerId() {
        return employerId;
    }

    public void setEmployerId(int employerId) {
        this.employerId = employerId;
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ProjectsID")

    public int getProjectsId() {
        return projectsId;
    }

    public void setProjectsId(int projectsId) {
        this.projectsId = projectsId;
    }

    @Basic
    @Column(name = "function", nullable=false)
    @NotNull( message = "Function must be not null")
    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    @Basic
    @Column(name = "startDate", nullable=false)
    @NotNull( message = "Start date must be not null")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable=false)
    @NotNull( message = "End date must be not null")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployerProjectsEntity that = (EmployerProjectsEntity) o;
        return employerId == that.employerId &&
                projectsId == that.projectsId &&
                Objects.equals(function, that.function) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(employerId, projectsId, function, startDate, endDate);
    }

    @ManyToOne (fetch=FetchType.LAZY,optional=false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }
    @ManyToOne (fetch=FetchType.LAZY,optional=false)
    public ProjectsEntity getProjectsByProjectsId() {
        return projectsByProjectsId;
    }

    public void setProjectsByProjectsId(ProjectsEntity projectsByProjectsId) {
        this.projectsByProjectsId = projectsByProjectsId;
    }

    @Override
    public String toString() {
        return "EmployerProjectsEntity{" +
                "employerId=" + employerId +
                ", projectsId=" + projectsId +
                ", function='" + function + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", employerByEmployerId=" + employerByEmployerId +
                ", projectsByProjectsId=" + projectsByProjectsId +
                '}';
    }
}
