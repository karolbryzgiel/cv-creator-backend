package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Client", schema = "", catalog = "")
public class ClientEntity {
    private int Id;
    private String name;
    private String address;
    private Collection<ProjectsEntity> projectsById;

    public ClientEntity(int id, String name, String address, Collection<ProjectsEntity> projectsById) {
        this.Id = id;
        this.name = name;
        this.address = address;
        this.projectsById = projectsById;
    }

    public ClientEntity() {
        this.Id = 0;
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    @Column(name = "Name", nullable=false)
    @NotNull( message = "Name must be not null")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "address", nullable=false)
    @NotNull( message = "Address must be not null")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientEntity that = (ClientEntity) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, address);
    }

    @OneToMany(mappedBy = "clientById")
    public Collection<ProjectsEntity> getProjectsById() {
        return projectsById;
    }

    public void setProjectsById(Collection<ProjectsEntity> educationsById) {
        this.projectsById = projectsById;
    }

    @Override
    public String toString() {
        return "ClientEntity{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
