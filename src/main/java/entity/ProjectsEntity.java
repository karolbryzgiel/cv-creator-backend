package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "Projects", schema = "", catalog = "")
public class ProjectsEntity {
    private int id;
    private String projectName;
    private String description;
    private Date startDate;
    private Date endDate;
    private ClientEntity clientByClientId;

    public ProjectsEntity(int id, String projectName, String description, Date startDate, Date endDate, ClientEntity clientByClientId) {
        this.id = id;
        this.projectName = projectName;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.clientByClientId = clientByClientId;
    }

    public ProjectsEntity() {
    }


    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "projectName", nullable=false)
    @NotNull( message = "Project name must be not null")
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Basic
    @Column(name = "description", nullable=false)
    @NotNull( message = "Description must be not null")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "startDate", nullable=false)
    @NotNull( message = "Start date must be not null")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable=false)
    @NotNull( message = "End date must be not null")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    @ManyToOne
    @JoinColumn(name = "clientId", referencedColumnName = "ID")
    public ClientEntity getClientById() {
        return clientByClientId;
    }

    public void setClientById(ClientEntity clientByClientId) {
        this.clientByClientId = clientByClientId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectsEntity that = (ProjectsEntity) o;
        return id == that.id &&
                Objects.equals(projectName, that.projectName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, projectName, description, startDate, endDate);
    }

    @Override
    public String toString() {
        return "ProjectsEntity{" +
                "id=" + id +
                ", projectName='" + projectName + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", clientByClientId=" + clientByClientId +
                '}';
    }
}
