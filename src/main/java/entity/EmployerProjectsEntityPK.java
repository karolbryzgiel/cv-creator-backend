package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

public class EmployerProjectsEntityPK implements Serializable {
    private int employerId;
    private int projectsId;

    @Column(name = "EmployerID")
    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    public int getEmployerId() {
        return employerId;
    }

    public void setEmployerId(int employerId) {
        this.employerId = employerId;
    }

    @Column(name = "ProjectsID")
    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    public int getProjectsId() {
        return projectsId;
    }

    public void setProjectsId(int projectsId) {
        this.projectsId = projectsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployerProjectsEntityPK that = (EmployerProjectsEntityPK) o;
        return employerId == that.employerId &&
                projectsId == that.projectsId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(employerId, projectsId);
    }
}
