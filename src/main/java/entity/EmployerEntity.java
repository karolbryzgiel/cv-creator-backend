package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Employer", schema = "", catalog = "")
public class EmployerEntity {
    private int id;
    private String pesel;
    private String name;
    private String surname;
    private Date birthDate;
    private String address;
    private String phoneNumber;
    private String emailAddress;
    private Date hiredate;

    @JsonIgnore
    private Collection<CertificationsEntity> certificationsById;
    @JsonIgnore
    private Collection<EducationEntity> educationsById;
    @JsonIgnore
    private Collection<EmployerProjectsEntity> employerProjectsById;
    @JsonIgnore
    private Collection<ExperienceEntity> experiencesById;
    @JsonIgnore
    private Collection<HobbiesEntity> hobbiesById;
    @JsonIgnore
    private Collection<SkillsEntity> skillsById;
    @JsonIgnore
    private Collection<UrlLinksEntity> urlLinksById;

    public EmployerEntity(String pesel, String name, String surname, Date birthDate, String address, String phoneNumber, String emailAddress, Date hiredate) {
        this.pesel = pesel;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.hiredate = hiredate;
    }

    public EmployerEntity() {
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy = GenerationType.TABLE,
            generator = "incrementByOne")
    @Column(name = "ID", nullable=false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "pesel", nullable=false, unique=true)
    @Size(min = 11, max = 11, message="Pesel must have 11 digits")
    @NotNull( message ="Pesel cant be not null")
    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    @Basic
    @Column(name = "name", nullable=false)
    @NotNull ( message="Name cant be not null")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "surname", nullable=false)
    @NotNull ( message="Surname cant be not null")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "birthDate", nullable=false)
    @NotNull ( message="Birthdate cant be not null")
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Basic
    @Column(name = "address", nullable=false)
    @NotNull ( message="Address cant be not null")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "phoneNumber", nullable=false)
    @NotNull ( message="Phone number cant be not null")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "emailAddress", nullable=false)
    @NotNull ( message="Email address cant be not null")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Basic
    @Column(name = "hiredate", nullable=false)
    @NotNull ( message="Hiredate cant be not null")
    public Date getHiredate() {
        return hiredate;
    }

    public void setHiredate(Date hiredate) {
        this.hiredate = hiredate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployerEntity that = (EmployerEntity) o;
        return id == that.id &&
                pesel == that.pesel &&
                phoneNumber == that.phoneNumber &&
                Objects.equals(name, that.name) &&
                Objects.equals(surname, that.surname) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(address, that.address) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(hiredate, that.hiredate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, pesel, name, surname, birthDate, address, phoneNumber, emailAddress, hiredate);
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<CertificationsEntity> getCertificationsById() {
        return certificationsById;
    }

    public void setCertificationsById(Collection<CertificationsEntity> certificationsById) {
        this.certificationsById = certificationsById;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<EducationEntity> getEducationsById() {
        return educationsById;
    }

    public void setEducationsById(Collection<EducationEntity> educationsById) {
        this.educationsById = educationsById;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<EmployerProjectsEntity> getEmployerProjectsById() {
        return employerProjectsById;
    }

    public void setEmployerProjectsById(Collection<EmployerProjectsEntity> employerProjectsById) {
        this.employerProjectsById = employerProjectsById;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<ExperienceEntity> getExperiencesById() {
        return experiencesById;
    }

    public void setExperiencesById(Collection<ExperienceEntity> experiencesById) {
        this.experiencesById = experiencesById;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<HobbiesEntity> getHobbiesById() {
        return hobbiesById;
    }

    public void setHobbiesById(Collection<HobbiesEntity> hobbiesById) {
        this.hobbiesById = hobbiesById;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<SkillsEntity> getSkillsById() {
        return skillsById;
    }

    public void setSkillsById(Collection<SkillsEntity> skillsById) {
        this.skillsById = skillsById;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employerByEmployerId")
    public Collection<UrlLinksEntity> getUrlLinksById() {
        return urlLinksById;
    }

    public void setUrlLinksById(Collection<UrlLinksEntity> urlLinksById) {
        this.urlLinksById = urlLinksById;
    }


    @Override
    public String toString() {
        return "EmployerEntity{" +
                "id=" + id +
                ", pesel=" + pesel +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                ", address='" + address + '\'' +
                ", phoneNumber=" + phoneNumber +
                ", emailAddress='" + emailAddress + '\'' +
                ", hiredate=" + hiredate +
                '}';
    }
}
