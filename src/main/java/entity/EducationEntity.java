package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "Education", schema = "", catalog = "")
public class EducationEntity {
    private int id;
    private Date startDate;
    private Date endDate;
    private String nameOfOrganisation;
    private String direction;
    private String specialization;
    private String professionalTitle;
    private EmployerEntity employerByEmployerId;

    public EducationEntity(int id, Date startDate, Date endDate, String nameOfOrganisation, String direction, String specialization, String professionalTitle, EmployerEntity employerByEmployerId) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.nameOfOrganisation = nameOfOrganisation;
        this.direction = direction;
        this.specialization = specialization;
        this.professionalTitle = professionalTitle;
        this.employerByEmployerId = employerByEmployerId;
    }

    public EducationEntity() {
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "startDate", nullable=false)
    @NotNull( message = "Start date must be not null")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable=false)
    @NotNull( message = "End date must be not null")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "nameOfOrganisation", nullable=false)
    @NotNull( message = "Name of organisation must be not null")
    public String getNameOfOrganisation() {
        return nameOfOrganisation;
    }

    public void setNameOfOrganisation(String nameOfOrganisation) {
        this.nameOfOrganisation = nameOfOrganisation;
    }

    @Basic
    @Column(name = "direction", nullable=false)
    @NotNull( message = "Direction must be not null")
    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Basic
    @Column(name = "specialization")
    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Basic
    @Column(name = "professionalTitle")
    public String getProfessionalTitle() {
        return professionalTitle;
    }

    public void setProfessionalTitle(String professionalTitle) {
        this.professionalTitle = professionalTitle;
    }


    @ManyToOne
    @JoinColumn(name = "EmployerID", referencedColumnName = "ID", nullable = false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }
}
