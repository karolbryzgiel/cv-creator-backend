package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "Experience", schema = "", catalog = "")
public class ExperienceEntity {
    private int id;
    private Date startDate;
    private Date endDate;
    private String companyName;
    private String position;
    private EmployerEntity employerByEmployerId;

    public ExperienceEntity(int id, Date startDate, Date endDate, String companyName, String position, EmployerEntity employerByEmployerId) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.companyName = companyName;
        this.position = position;
        this.employerByEmployerId = employerByEmployerId;
    }

    public ExperienceEntity() {
    }


    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "startDate", nullable=false)
    @NotNull( message = "Start date must be not null")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable=false)
    @NotNull( message = "End date must be not null")
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "companyName", nullable=false)
    @NotNull( message = "Company name must be not null")
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Basic
    @Column(name = "position", nullable=false)
    @NotNull( message = "Position must be not null")
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @ManyToOne
    @JoinColumn(name = "EmployerID", referencedColumnName = "ID", nullable = false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }

    @Override
    public String toString() {
        return "ExperienceEntity{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", companyName='" + companyName + '\'' +
                ", position='" + position + '\'' +
                ", employerByEmployerId=" + employerByEmployerId +
                '}';
    }
}
