package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "Skills", schema = "", catalog = "")
public class SkillsEntity {
    private int id;
    private String name;
    private String level;
    private EmployerEntity employerByEmployerId;

    public SkillsEntity(int id, String name, String level, EmployerEntity employerByEmployerId) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.employerByEmployerId = employerByEmployerId;
    }

    public SkillsEntity() {
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable=false)
    @NotNull( message = "Name must be not null")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "level", nullable=false)
    @NotNull( message = "Level must be not null")
    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    @ManyToOne
    @JoinColumn(name = "EmployerID", referencedColumnName = "ID", nullable = false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }

    @Override
    public String toString() {
        return "SkillsEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                ", employerByEmployerId=" + employerByEmployerId +
                '}';
    }
}
