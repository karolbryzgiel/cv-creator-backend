package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "Certifications", schema = "", catalog = "")
public class CertificationsEntity {
    private int id;
    private Date dateofGet;
    private String name;
    private EmployerEntity employerByEmployerId;

    public CertificationsEntity(Date dateofGet, String name, EmployerEntity employerByEmployerId) {
        this.dateofGet = dateofGet;
        this.name = name;
        this.employerByEmployerId = employerByEmployerId;
    }

    public CertificationsEntity() {
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dateofGet", nullable=false)
    @NotNull( message = "Date of get must be not null")
    public Date getDateofGet() {
        return dateofGet;
    }

    public void setDateofGet(Date dateofGet) {
        this.dateofGet = dateofGet;
    }

    @Basic
    @Column(name = "name", nullable=false)
    @NotNull( message = "Name of link must be not null")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @ManyToOne
    @JoinColumn(name = "EmployerID", referencedColumnName = "ID", nullable = false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }

    @Override
    public String toString() {
        return "CertificationsEntity{" +
                "id=" + id +
                ", dateofGet=" + dateofGet +
                ", name='" + name + '\'' +
                ", employerByEmployerId=" + employerByEmployerId +
                '}';
    }
}
