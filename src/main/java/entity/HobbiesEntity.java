package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "Hobbies", schema = "", catalog = "")
public class HobbiesEntity {
    private int id;
    private String category;
    private String name;
    private EmployerEntity employerByEmployerId;

    public HobbiesEntity(int id, String category, String name, EmployerEntity employerByEmployerId) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.employerByEmployerId = employerByEmployerId;
    }

    public HobbiesEntity() {
    }


    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "category", nullable=false)
    @NotNull( message = "Category must be not null")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Basic
    @Column(name = "name", nullable=false)
    @NotNull( message = "Name must be not null")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @ManyToOne
    @JoinColumn(name = "EmployerID", referencedColumnName = "ID", nullable = false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }

    @Override
    public String toString() {
        return "HobbiesEntity{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", employerByEmployerId=" + employerByEmployerId +
                '}';
    }
}
