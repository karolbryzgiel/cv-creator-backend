package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "urlLinks", schema = "", catalog = "")
public class UrlLinksEntity {
    private int Id;
    private String linkName;
    private String url;
    private EmployerEntity employerByEmployerId;

    public UrlLinksEntity(int id, String linkName, String url, EmployerEntity employerByEmployerId) {
        this.Id = id;
        this.linkName = linkName;
        this.url = url;
        this.employerByEmployerId = employerByEmployerId;
    }

    public UrlLinksEntity() {
    }

    @TableGenerator(
            name = "incrementByOne",
            allocationSize = 1,
            initialValue = 1)
    @Id
    @GeneratedValue(
            strategy=GenerationType.TABLE,
            generator="incrementByOne")

    @Column(name = "Id")
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    @Column(name = "linkName", nullable=false)
    @NotNull( message = "Link name must be not null")
    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    @Basic
    @Column(name = "url", nullable=false)
    @NotNull( message = "URL must be not null")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @ManyToOne
    @JoinColumn(name = "EmployerID", referencedColumnName = "ID", nullable = false)
    public EmployerEntity getEmployerByEmployerId() {
        return employerByEmployerId;
    }

    public void setEmployerByEmployerId(EmployerEntity employerByEmployerId) {
        this.employerByEmployerId = employerByEmployerId;
    }

    @Override
    public String toString() {
        return "UrlLinksEntity{" +
                "Id=" + Id +
                ", linkName='" + linkName + '\'' +
                ", url='" + url + '\'' +
                ", employerByEmployerId=" + employerByEmployerId +
                '}';
    }
}
